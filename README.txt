=====
Block Headline
-----
Block Headline was developed and is maintained by Penceo
<http://www.penceo.com>.


=====
Installation
-----

1. Enable the module
2. To change the block headline tag, simply visit the block's
configuration page at
Administration > Structure > Blocks
